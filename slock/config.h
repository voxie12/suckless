 /*      _            _    */
/*  ___| | ___   ___| | __ */
/* / __| |/ _ \ / __| |/ / */
/* \__ \ | (_) | (__|   <  */
/* |___/_|\___/ \___|_|\_\ */

static const char *user  = "nobody";
static const char *group = "wheel";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "black",     /* after initialization */
	[INPUT] =  "#181825",   /* during input */
	[FAILED] = "#f7768e",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* default message */
static const char * message = "go away";

/* text color */
static const char * text_color = "#a9b1d6";

/* text size (must be a valid size) */
static const char * font_name = "6x13";

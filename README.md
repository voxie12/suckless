# Suckless
dwm is a dynamic window manager for X. It manages windows in tiled, monocle and floating layouts. All of the layouts can be applied dynamically, optimising the environment for the application in use and the task performed.

patches are hell

![](https://i.imgur.com/OF7HvFf.png)
## The software I use:
- dwm
- dmenu
- st
- slstatus
- slock
- surf

## The patches I use:

### dwm -
- attachbottom
- alwayscenter
- fullgap
- swallow

### st -
- alpha
- scrollback
- scrollback-mouse

### surf
- websearch


### slock -
- message

### dmenu -
- n/a

### slstatus -
- n/a



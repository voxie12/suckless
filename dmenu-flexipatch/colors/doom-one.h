static const char *colors[][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#cccccc", "#282c34" },
	[SchemeSel] = { "#1c1f24", "#c678dd" },
	[SchemeSelHighlight] = { "#98be65", "#000000" },
	[SchemeNormHighlight] = { "#98be65", "#000000" },
};


